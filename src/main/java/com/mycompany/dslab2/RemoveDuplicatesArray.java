/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dslab2;

/**
 *
 * @author test1
 */
import java.util.Arrays;

public class RemoveDuplicatesArray {
    public static void main(String[] args) {
        int[] nums = {1, 1, 2, 2, 2, 3, 3, 4, 4, 4, 4 , 5};
        int[] uniqueNums = removeDuplicates(nums);
        int count = 0;
        for (int i = 0; i < uniqueNums.length; i++) {
            if (uniqueNums[i] != 0) {
                count++;
            }
        }
        
        System.out.print(count + ", ");
        for (int num : uniqueNums) {
            if (num != 0) {
                System.out.print(num + " ");
            }
        }
        for (int i = 0; i < nums.length - count; i++) {
            System.out.print("_ ");
        }
        System.out.println();
    }

    private static int[] removeDuplicates(int[] arr) {
        Arrays.sort(arr); // Sort the array to group duplicates together
        int[] uniqueArr = new int[arr.length];
        int uniqueCount = 0;

        for (int i = 0; i < arr.length; i++) {
            if (i == 0 || arr[i] != arr[i - 1]) { // Check for duplicates
                uniqueArr[uniqueCount++] = arr[i];
            }
        }

        return uniqueArr;
    }
}


