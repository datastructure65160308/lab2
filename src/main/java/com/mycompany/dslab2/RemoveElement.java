/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dslab2;


/**
 *
 * @author test1
 */
public class RemoveElement {
    public static void main(String[] args) {
        int[] nums = {3,2,2,3,4,5,4,3,2,3,10} ;
        int val = 3 ;
        printArray(nums);
        deleteAllValueArray(nums,val);
        
    }
    
    // print array method
    private static void printArray(int[] array) {
        for (int i=0 ; i<array.length ; i++){
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    private static void deleteAllValueArray(int[] arr ,int val) {
        int count = 0 ;
        for (int i=0 ;i<arr.length ;i++){
            if (arr[i]==val){
                count++ ;
            } 
        }
        int[] newArr = new int[arr.length - count] ;        
        for (int i = 0,j=0; i<arr.length ;i++){
            if (arr[i] != val){
                newArr[j] = arr[i];
                j++;
            }
        }
        System.out.print(arr.length - count+", ");
        for (int i=0 ; i<newArr.length ;i++){
            System.out.print(newArr[i] + " ");
        }
        for (int i=0 ; i<count ;i++){
            System.out.print("_ ");
        }
        System.out.println();
    }
    
    
}
