/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.dslab2;

/**
 *
 * @author test1
 */
public class ArrayDeletionsLab {

    public static void main(String[] args) {
        // Original Array
        int[] numbers = {1,2,3,4,5} ;
        
        // New Array // Pick index to delete
        int[] newArray ;
        newArray = deleteElementByIndex(numbers,3);
        
        // Original and New of Array print
        printOrinalAndNewArray(numbers, newArray);
        
        // New Array // Pick value to delete
        int[] new2Array = deleteElementByValue(newArray, 3);
        
        // Original and New of Array print
        printOrinalAndNewArray(newArray, new2Array);        
  
    }

    private static void printOrinalAndNewArray(int[] numbers, int[] newArray) {
        // print Original Array
        System.out.println("Original Array");
        printArray(numbers);
        // print New Array
        System.out.println("New Array");
        printArray(newArray);
    }
        // print array method
        private static void printArray(int[] array) {
        for (int i=0 ; i<array.length ; i++){
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    private static int[] deleteElementByIndex(int[] arr, int index) {
        // New Array
        int[] newArray = new int[arr.length - 1] ;
         // Delete index method process
        for (int i=0; i<index; i++){
            newArray[i] = arr[i] ; 
        }
        
        for (int i=index ;i<newArray.length ;i++){
            newArray[i] = arr[i+1] ;
        }
        
        return newArray ;
    }

    private static int[] deleteElementByValue(int[] arr, int value) {
        for (int i=0 ; i<arr.length ;i++){
            if (arr[i] == value){
                return deleteElementByIndex(arr, i) ;
            }
        }
        return arr ;
    }
}
